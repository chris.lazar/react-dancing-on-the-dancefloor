import React, { Component } from 'react';
import RenderDanceFloor from '../RenderDanceFloorCells/index.js';
import './index.scss';

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: true,
        }
    }

    counterUpdate = () => {
        this.setState(prevState => {
            return {
                active: !prevState.active
            }
        })
    }

    id = () => (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase()

    render() {
        return (
            <div className="Container">
                <div className="DanceFloor-Container">
                    {<RenderDanceFloor id={this.id} changeState={this.counterUpdate} />}
                </div>
            </div>
        );
    }
}

export default App;