import React, { Component } from 'react';
import Cell from '../../components/Cell/index.js';

class RenderDanceFloor extends Component {
    constructor(props) {
        super(props);
        this.cells = [];
        this.createCells();
        this.clicked = true;
    }

    createCells = () => {
        let counter = 0;
        for (let i = 0; i < 5; i++) {
            for (let j = 0; j < 5; j++) {
                counter += 1;
                this.cells.push({ class: `not-active`, id: counter, ID: this.props.id() })
            }
        }
    }

    clickCell = e => {
        if (this.clicked) {
            e.class === 'active' ? e.class = 'not-active' : e.class = 'active';
            this.props.changeState();
            this.clicked = false;
        }
    }

    edgeCells = (active, keyCode) => {
        if (active.id === 1) {
            return this.cells.filter(cell => cell.id === active.id + 24)
        }
        else if (active.id === 25) {
            return this.cells.filter(cell => cell.id === 1)
        }
        if (keyCode === 38) {
            return this.cells.filter(cell => cell.id === active.id + 20)
        }
        else if (keyCode === 40) {
            return this.cells.filter(cell => cell.id === active.id - 20)
        }
        return undefined;
    }
    activateCell = (steps, keyCode) => {
        const active = this.cells.filter(cell => cell.class === 'active')[0]
        const nextCell = this.cells.filter(cell => cell.id === active.id + steps)

        if (nextCell.length !== 0) {
            nextCell[0].class = 'active';
            active.class = 'not-active'
            this.props.changeState();
        }
        if (nextCell.length === 0) {
            this.edgeCells(active, keyCode)[0].class = 'active';
            active.class = 'not-active'
            this.props.changeState();
        }
    }

    moveCell = (e) => {
        if (e.keyCode === 40) this.activateCell(5, e.keyCode);
        else if (e.keyCode === 38) this.activateCell(-5, e.keyCode);
        else if (e.keyCode === 39) this.activateCell(1);
        else if (e.keyCode === 37) this.activateCell(-1);
    }

    renderCells = () => {
        return this.cells.map(danceFloor => {
            return (
                <Cell
                    key={danceFloor.ID}
                    clicked={() => this.clickCell(danceFloor)}
                    moveCell={(e) => this.moveCell(e)}
                    danceFloorClass={danceFloor.class}
                />
            )
        })
    }

    render() {
        return this.renderCells()
    }
}

export default RenderDanceFloor;