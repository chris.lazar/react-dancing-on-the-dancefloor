import React from 'react';
import './index.scss';

const cell = (props) => (
    <div
        className={`Cell ${props.danceFloorClass}`}
        onClick={props.clicked}
        role="button"
        tabIndex="0"
        onKeyDown={props.moveCell}
    />
)


export default cell;